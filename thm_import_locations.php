<?php

require_once('/opt/WebUI/config/config.php');
require_once('AuthenticationService/service/ASA_AuthSession.php');
require_once('ResourceService/service/RES_ResourceServiceSession.php');
#require_once('ResourceService/service/search/RES_ResourceObjectSearch.php');
require_once('ResourceService/service/search/RES_ResourceFlySearch.php');
require_once('ResourceService/service/search/RES_ResourceConditionManager.php');
require_once('objects/WebUI_Location.php');
require_once('objects/WebUI_DB.php');
require_once('objects/WebUI_User.php');
require_once('objects/WebUI_ConnectionList.php');
require_once('objects/WebUI_Connection.php');
require_once('objects/WebUI_MarketingZoneList.php');
require_once('objects/WebUI_MarketingZone.php');
require_once('objects/WebUI_MZDisplayConfiguration.php');
require_once('objects/WebUI_Device.php');
require_once('objects/WebUI_MZOperations.php');
require_once('objects/WebUI_TimezoneList.php');
require_once('objects/WebUI_LocationOperationManage.php');
/**
 * Support Scripts Location : Location Import
 * Works for THM clients only
 * @author jperez
 * Editted by JPost
 *
 * v 0.2
 * CHANGELOG
 *  + Will not create DMB connection if not being requested for DMB MZs. 20160504 FA.
*/

$clientid = '288';
$clientname = 'Tim Hortons Master';

class SS_Location extends WebUI_Location {
	protected $newinstances;
	protected $current_instance_id;

	protected $user_id;
	protected $client_id;

	protected $connection;
	protected $mzs;
	/**
	 * Startup application
	 */
	public function __construct($client_id) {
		$this->user_id = 4;
		$this->client_id = $client_id;
		$link = WebUI_DB::newConnect();
		$config = CM_ConfigFactory::getInstance('webui_services');
		$session = new ASA_AuthSession($config->getValue('instance_name', 'webui_services'));
		RES_ResourceServiceSession::initSession(array(), $session);
		$session->addSession('RES_ResourceServiceSession');
		parent::__construct($link);
	}
	private function fillSSLocation($inst=array()){
		// reset this instance
		$this->instance = new RES_Resource;
		$this->instance->setType('location');
		// set field values
		$this->setClientId($this->client_id);
		$this->setName(trim($inst['name']));
		$this->setPhoneNumber($inst['phone']);
		$this->setAddress(ucwords(strtolower($inst['address'])));
		$this->setCountryName($inst['country']);
		$this->setProvinceName($inst['province']);
		$this->setCityName(ucwords(strtolower($inst['city'])));
		$this->setPostalCode(trim($inst['postal']));
		$this->setDistributionCentreSetting(0);
		$this->setAirportCodeName('YXU London');
		$this->setTimezoneId($inst['tz_id']);
	}
	private function printDiff($inst=array(), $onlyDiff=false){
		$values = array(
				$inst['name']      => $this->getName(),
				$inst['address']   => $this->getAddress(),
				$inst['phone']     => $this->getPhoneNumber(),
				$inst['country']   => $this->getCountryName(),
				$inst['province']  => $this->getProvinceName(),
				$inst['city']      => $this->getCityName(),
				$inst['postal']    => $this->getPostalCode(),
				$inst['tz_id']     => $this->getTimezoneId()
		);
		$this->debug("Value from .CSV -- Value from Xpress", "INFO");
		foreach ($values as $csv=>$obj) $this->debug(sprintf("\t [%s] -- [%s]", $csv, $obj), "INFO");
	}
	/**
	 * Load location instance into Object to validate existence
	 *
	 * @param Array $inst
	 * @return boolean
	 */
	public function checkLocation($location_name=""){
		$location_name = $this->newinstances[$this->current_instance_id]['name'];
		$loc = $this->getLocationByName($location_name);
		if(!empty($loc)){
			$this->load($loc->getId());
			return true;
		}
		// New location found
		$this->fillSSLocation($this->newinstances[$this->current_instance_id]);
		return false;
	}
	/**
	 * REsource loader by Name
	 * @param string $location_name
	 * @return RES_Resource | boolean
	 */
	private function getLocationByName($location_name){
		preg_match('/\d{5,6}/', $location_name, $id);
		$id = (is_array($id) && (count($id)>=1)) ? $id[0] : $location_name;
		$search = new RES_ResourceObjectSearch;
		$search->getConditionManager()->setTargetType('location');
		$search->getConditionManager()->setClientIds(array($this->client_id));
		$search->getConditionManager()->setCondition('name', trim($id), RES_ResourceConditionManager::OP_EQ);
		// Location already exists
		if($search->getCount() >= 1){
			return array_pop($search->getResults());
		}
		return false;
	}
	/**
	 * Config and Save NEW location
	 * @param int $client_id
	 * @param array $location
	 * @return boolean
	 */
	public function addLocation($doit=false){
		$status = false;
		$status = $this->save($this->user_id);
		if($status){
			// setting GP id for location
			$loc_resource = RES_ResourceLoader::loadById($this->getId());
			$loc_resource->setGpAddressId($this->newinstances[$this->current_instance_id]['gp_id']);
			$loc_resource->save();
		}
		return $status;
	}
	/**
	 * Verify existing connection on location
	 * @param string $conn_id
	 * @param string $conn_name
	 * @return boolean
	 */
	public function checkConnection($conn_name, $save=true){
		// check DMB existence
		$fdmb_conn = $this->findConnection($conn_name);
		if(!empty($fdmb_conn)) {
			// DMB connection already exists
			if($save) $this->connection = $fdmb_conn;
			// loading MZ names / print MZs under connection
			$mzs = new WebUI_MarketingZoneList($this->getDB());
			$mzs->addWhere('connection', $fdmb_conn->getId());
			$mzs->addWhere('deleted', 0);
			$mzs->load();
			$mz_names = $mzs->getDataArray('getName');
			$this->debug(sprintf("Connection configured with MZs: [%s]", implode(",", $mz_names)), "INFO");
			//
			return $fdmb_conn;
		}
		return 0;// ready to createConnection
	}
	private function findConnection($conn_name){
		// Load connection names
		$connections= new WebUI_ConnectionList($this->getDB());
		$connections->addWhere('location', $this->getId());
		$connections->load();
		if($connections->hasError()) $this->debug($connections->getError()->getMsg(), "ERROR");
		if($connections->count() > 0){
			// one connection found
			return $connections->findValue('getName', $conn_name);
		}
		return false;
	}
	/**
	 * Create new Connection
	 * @param string $conn_name
	 * @return boolean
	 */
	public function addConnection($conn_name, $save=true, $provider="Radiant", $status="PENDING", $carrier="other"){
		$connection = new WebUI_Connection($this->getDB());
		$connection->setClientId($this->client_id);
		$connection->setLocationId($this->getId());
		$connection->setName($conn_name);
		$connection->setProviderName($provider);
		$connection->setTypeName("Other");
		$connection->setStatusName($status);
		$connection->setCarrierName($carrier);
		$connection->setReportDefaultSetting(1);
		$status = $connection->save($this->user_id);
		$fdmb_conn = $this->findConnection($conn_name);
		if(!empty($fdmb_conn)) {
			// update this connection with new Connection created
			if($save) $this->connection = $fdmb_conn;
			return $fdmb_conn;
		}
		return 0;
	}
	/**
	 * Create new Marketing Zone
	 * @param number $num
	 * @param string $netMask
	 * @param string $getway
	 * @param string $ip
	 * @return boolean
	 */
	public function addMZs($conn_id=NULL, $mz_count=NULL, $mz_name=NULL) {

		$resource_ids = $this->getPlaceholderList();
		$current_serial = $this->placeholdersInit();
		$type = $this->getEntity('device type', array('name' => 'Placeholder'));
		$model = $this->getEntity('device model', array('name' => 'ME3'));
		$os = $this->getEntity('os info', array('name' => 'Xpress-c3_ph-1_0_0-RELEASE'));
		if(empty($conn_id)) $conn_id = $this->connection->getId();
		if(empty($mz_count)) $mz_count=$this->newinstances[$this->current_instance_id]['mz_count'];
		$ip=$this->newinstances[$this->current_instance_id]['ip_address'];
		$lastip=$this->newinstances[$this->current_instance_id]['last_ip'];

		$check_existence=FALSE;

		$mzs = new WebUI_MarketingZoneList($this->getDB());
		$mzs->addWhere('connection', $conn_id);
		$mzs->load();
		if($mzs->count()>0)$check_existence=TRUE;
		
		// Determining if the CSV is asking for more MZ's then are currently set up for an already
		// existing location.

		$i = 1;
		if ($mzs->count() > $mz_count) $i = ($mz_already_set_up + 1);
		if ($mzs->count() >= $mz_count){
			echo "\n\t\tMZ's are already set up for this connection.";
			$this->debug(sprintf("There are already %d MZ(s) set up for this connection. %d were requested. Skipping setting up any additional MZ's for this connection.", $mzs->count(), $$mz_count), "INFO");
			return 1;
		} 
		
		$mz_basename = $mz_name;
		for($i=$i;$i<=$mz_count;$i++) {
			// create MZ
			if(empty($mz_name)) $mz_name = sprintf("Screen %d", $i);
			if(preg_match("/TimsTV/", $mz_basename)){
				$alpha = $this->__getAlpha($i);
				$mz_name = sprintf("%s %s", $mz_basename, $alpha);
			} 
			if(preg_match("/Presell/", $mz_basename)){
                                $alpha = $this->__getAlpha($i);
                                $mz_name = sprintf("%s %s", $mz_basename, $alpha);
                        } 
			$mz = new WebUI_MarketingZone($this->getDB());
			$mz->setClientId($this->client_id);
			$mz->setConnectionId($conn_id);
			$mz->setName($mz_name);
			// check existing MZ
			$existing_mz = $mzs->findValue('getName', $mz_name);
			if($check_existence && !empty($existing_mz)){
				// log duplicate configuration
				$old_ip   = $existing_mz->getNetworkAddress();
				$old_mask = $existing_mz->getNetworkMask();
				$old_gate = $existing_mz->getNetworkGateway();
				$this->debug("$mz_name already configured for this connection.\n\tIP=$old_ip\n\tMASK=$old_mask\n\tGATE=$old_gate", "INFO");
				$mz_name=NULL;
				continue;
			} else{
				if (!empty($ip)){
					$ip_add = $this->setIP($i, $ip, 129);
					$ip_gate = $this->newinstances[$this->current_instance_id]['ip_gateway'];
					$ip_mask = $this->newinstances[$this->current_instance_id]['ip_mask'];
					$mz->setNetworkAddress($ip_add);
					$mz->setNetworkGateway($ip_gate);
					$mz->setNetworkMask($ip_mask);
				 	$this->debug("Setting $mz_name as next:\n\tIP=$ip_add\n\tMASK=$ip_mask\n\tGATE=$ip_gate", "INFO");
				} else {	
					$this->debug("Setting $mz_name.  NOT setting any static IP addresses for this client", "INFO");
				}
			}
			$lom = new WebUI_LocationOperationManage($this->getDB());
			$lom->load($this->getId());
			if(!$mz->validate()){
				$this->debug("MZ Validation failure", "ERROR");break;
			}
			if(!$mz->save($this->user_id)){
				$this->debug("Problems creating MZ", "ERROR");
				return false;
			}
			$apply = new WebUI_OperationApply($this->getDB());
			$apply->setOperationManager($lom->getOperationManager());
			$user = new WebUI_User($link);
			$user->load($this->user_id);
			if(!$apply->applyToMZ($mz, $user)) {
				$this->debug("Problems creating MZ", "ERROR");
				return false;
			}
			// setting up master/slave
			$playType='MASTER';
			if($i>1)$playType=sprintf("SLAVE %d",($i-1));
			if(preg_match("/TimsTV/", $mz_basename))$playType='MASTER';
			if(preg_match("/Presell/", $mz_basename))$playType='MASTER';
			$mz->setPlayType($playType);$this->debug("Setting up $mz_name as $playType", "INFO");
			$op = $mz->getOperationManager();
			// setting display schedule = Always On - ref:/opt/WebUI/lib/WebUI_OperationManage.php
			$op->setDisplayTimes(array(array('on'  => '00:00:00','off' => '00:00:00','days'=> 127)));
			// setting election enabled  = Yes and NO for McCafe MZs
			if(preg_match("/Cafe/", $mz_name)) $op->setElectionEnabled(0);
			else $op->setElectionEnabled(0);
			$op->setPlaybackMode('BLIPLESS');
			if(!$op->save())$this->debug("Problems updating MZOperationManager", "ERROR");

			$mz->setOperationManager($op);
			if(!$mz->save($this->user_id))$this->debug("Problems updating MZ", "ERROR");
			// sending IP package
			$static_package = new WebUI_StaticNetworkPackage($this->getDB());
			$static_package->build($mz, $this->user_id);
			// display configuration
			$display = new WebUI_MZDisplayConfiguration($this->getDB());
			$display->load($mz->getId());
			// setting screentype to 1 = LG (LG, Samsung, Sharp, etc.)
			$display->setDisplayConfiguration(1, 'ON', 'LOCKED', 80);
			if(!$display->save())$this->debug("Problems updating MZDisplayConfiguration", "ERROR");
			// adding place holder to new MZ
			$this->debug(sprintf("Associating placeholders for MZ [%s]", $mz->getName()), "INFO");
			$dev_id = 0;
			if (count($resource_ids) == 0) {
				$this->debug(sprintf("Creating new placeholder [FD1-%05d]", $current_serial), "INFO");
				$dev_id = $this->getPlaceholderId($current_serial, $os, $model, $type);
				$current_serial++;
			} else {
				$this->debug(sprintf("Loading unused fake-device 1 of %d", count($resource_ids)), "INFO");
				// get fake device from list
				$dev_id = array_pop($resource_ids);
			}
			if(!$this->addPlaceholder($mz, $dev_id, $user)){
				$this->debug("\tCan't create placeholders", "ERROR");
			}
			$this->debug(sprintf("[ID:%d] attached to [%s]", $dev_id, $mz->getName()), "INFO");
			$mz_name=NULL;
		}
		return true;
	}
	private function __getAlpha($n){
		$alphabet   = array('A','B','C','D','E','F','G','H','I','J','K','L','M','N','O','P','Q','R','S','T','U','V','W','X','Y','Z');
   		$n = $n-1;
    		if($n <= 26){
        		$alpha =  $alphabet[$n];
		} else {
			$this->debug("\tCan't create MZ - there appears to be more than 26 of them and I have run out of letters to append to the them", "ERROR");
		}
		return $alpha;
	}
	private function setIP($screen_index, $ip, $end=null){
		$octet = explode(".", $ip);
		if(empty($end))$end=$octet[3];
		if(count($octet)==4) return sprintf("%d.%d.%d.%d",$octet[0],$octet[1],$octet[2],($end+$screen_index));
		return "$ip";
	}
	public function addPlaceholder($mz, $dev_id, $user){
		$device = new WebUI_Device($this->getDB());
		$device->load($dev_id);
		// Ensure fake device is set to PENDING
		$device->setStatusId('PENDING');
		$device->update();
		//Configure device and packages
		$operation = new WebUI_MZOperations($this->getDB());
		// do associate
		return $operation->addDevice($mz, $device, $user);
	}
	private function getPlaceholderId($current_serial, $os, $model, $type, $prefix="FD1-"){
		// Creating new fake device
		$resource = new RES_Resource();
		$resource->setType('device');
		$resource->setClientId($this->client_id);
		$resource->setName(sprintf('%s%05d', $prefix, $current_serial));
		$resource->setModel($model->getId());
		$resource->setEntityValues('type', array($type->getId()));
		$resource->setOs($os->getId());
		$resource->setStatus('PENDING');
		$resource->save();
		return $resource->getId();
	}
	private function placeholdersInit($prefix="FD1-"){
		$latest = null;
		$resource_search = new RES_ResourceItemSearch();
		$resource_search->getConditionManager()->setTargetType('device');
		$resource_search->getConditionmanager()->setCondition('name', $prefix);
		$resource_search->getConditionmanager()->setClientIds(array($this->client_id));
		$resource_search->setPageLength(1000);
		for($i=1;$i<=$resource_search->getPageCount();$i++) {
			$resource_search->setCurrentPage($i);
			$results = $resource_search->getResults();
			foreach($results as $result) {
				if(empty($latest) || $result->getId() > $latest->getId())$latest = $result;
			}
		}
		if(empty($latest)) {
			$offset = 0;
		} else {
			$pattern = array();
			$pattern = sprintf('/^%s(\d\d\d\d\d).*$/', $prefix);
			if(!preg_match($pattern, $latest->getName(), $matches)) $this->debug(sprintf("\tI found %s but that doesn't match our regex", $latest->getName()),"INFO");
			$offset = (int)$matches[1];
		}
		$current_serial = $offset+1;
		return $current_serial;
	}
	private function getPlaceholderList($prefix="FD1-"){
		$resource_ids = array();
		$resource_search = new RES_ResourceItemSearch();
		$resource_search->getConditionManager()->setTargetType('device');
		$resource_search->getConditionManager()->setCondition('name', $prefix);
		$resource_search->getConditionmanager()->setClientIds(array($this->client_id));
		$resource_search->getConditionManager()->setResultState(RES_ResourceConditionManager::ARCH_ACTIVE);
		$resource_search->getConditionManager()->setCondition('operation', '', RES_ResourceConditionManager::OP_HAS, 'marketing zone', RES_ResourceConditionManager::COND_EXC);
		$resource_search->setPageLength(1000);
		for($i=1;$i<=$resource_search->getPageCount();$i++) {
			$resource_search->setCurrentPage($i);
			$resource_ids = array_merge($resource_ids, COM_ArrayHelper::extract($resource_search->getResults(), 'getId'));
		}
		sort($resource_ids);
		return $resource_ids;
	}
	private function getEntity($type, $values) {
		$found = false;
		$entities = RES_EntityLoader::loadList($type);
		foreach($entities as $entity) {
			foreach($values as $name => $value) {
				$found = false;
				if($entity->getPropertyValue($name) != $value)
					break;
				$found = true;
			}
		}
		if(!$found) {
			$entity = new RES_Entity();
			$entity->setType($type);
			foreach($values as $name => $value)
				$entity->setPropertyValues($name, $value);
			$entity->save();
		}
		return $entity;
	}

	/** This function gives a variable a default variable if it is null
	 *
	 *
         *
         */

	public function defaultVariable($name = null, $default = 0){
		$name = $name ? $name : "$default";
		return $name;
	}
	/**
	 * Process CSV values and format it to Array(Locations)
	 * @param Array $data
	 * @return Array(Locations)
	 */
	public function formatCsv($data){
		$location_instances = array();
		$p_lookup = array (
			// Provinces:
                                'QC' => 'Quebec',
                                'ON' => 'Ontario',
                                'NS' => 'Nova Scotia',
                                'NB' => 'New Brunswick',
                                'MB' => 'Manitoba',
                                'NL' => 'Newfoundland',
                                'AB' => 'Alberta',
                                'BC' => 'British Columbia',
                                'NT' => 'Northwest Territories',
                                'NU' => 'Nunavut',
                                'PE' => 'Prince Edward Island',
                                'SK' => 'Saskatchewan',
                                'YT' => 'Yukon',
                        // States:
                                'AL' => 'Alabama',
                                'AK' => 'Alaska',
                                'AZ' => 'Arizona',
                                'AR' => 'Arkansas',
                                'CA' => 'California',
                                'CO' => 'Colorado',
                                'CT' => 'Connecticut',
                                'DC' => 'District of Columbia',
                                'DE' => 'Delaware',
                                'FL' => 'Florida',
                                'GA' => 'Georgia',
                                'HI' => 'Hawaii',
                                'ID' => 'Idaho',
                                'IL' => 'Illinois',
                                'IN' => 'Indiana',
                                'IA' => 'Iowa',
                                'KS' => 'Kansas',
                                'KY' => 'Kentucky',
                                'LA' => 'Louisiana',
                                'ME' => 'Maine',
                                'MD' => 'Maryland',
                                'MA' => 'Massachusetts',
                                'MI' => 'Michigan',
                                'MN' => 'Minnesota',
                                'MS' => 'Mississippi',
                                'MO' => 'Missouri',
                                'MT' => 'Montana',
                                'NE' => 'Nebraska',
                                'NV' => 'Nevada',
                                'NH' => 'New Hampshire',
                                'NJ' => 'New Jersey',
                                'NM' => 'New Mexico',
                                'NY' => 'New York',
                                'NC' => 'North Carolina',
	                        'OH' => 'Ohio',
                                'OK' => 'Oklahoma',
                                'OR' => 'Oregon',
                                'PA' => 'Pennsylvania',
                                'RI' => 'Rhode Island',
                                'SC' => 'South Carolina',
                                'SD' => 'South Dakota',
                                'TN' => 'Tennessee',
                                'TX' => 'Texas',
                                'UT' => 'Utah',
                                'VT' => 'Vermont',
                                'VA' => 'Virginia',
                                'WA' => 'Washington',
                                'WV' => 'West Virginia',
                                'WI' => 'Wisconsin',
                                'WY' => 'Wyoming'
		);
		$t_lookup = array (
				'Nova Scotia'           => 'Atlantic',
				'New Brunswick'         => 'Eastern',
				'Manitoba'              => 'Central',
				'Newfoundland'          => 'Newfoundland',
				'Alberta'               => 'Mountain',
				'British Columbia'      => 'Pacific',
				'Northwest Territories' => 'Mountain',
				'Nunavut'               => 'Eastern',
				'Prince Edward Island'  => 'Newfoundland',
				'Saskatchewan'          => 'Central',
				'Yukon'                 => 'Pacific',
				'Ontario'               => 'Eastern',
				'Quebec'                => 'Eastern'
		);
		$c_lookup = array(
				'CAN' => 'Canada',
				'USA' => 'USA',
				'United States' => 'USA'
		);
		$timezones = new WebUI_TimezoneList($this->getDB());
		$timezones->load();
		$tz_lookup = array();
		while($tz = $timezones->fetchItem())$tz_lookup[$tz->getName()] = $tz->getId();
                echo "Location Name\t\tAddress\t\t\tCity\t\tProv/State\tCountry\t\tPostal/Zip\tPhone\t\tFDMBs\tTimsTV\t DT \tPrint\n";
                echo "-------------\t\t-------\t\t\t----\t\t----------\t-------\t\t----------\t-----\t\t-----\t------\t----\t-----\n";
		foreach($data as $i=>$line) {
			if ($i == 0)continue;// skip headers
			
			$national_name=trim($this->__getValue($line, 0));

			// Need to find the country value so we can properly set the national name.
                        //   If the location is Canadian, than the location ID is 1#####
                        //   if the location is American, than the location ID is 9#####

                        $country  = (isset($c_lookup[$line[5]])) ? $c_lookup[$line[5]] : $line[5];

			// Validate national name & Format nation name
			if(empty($national_name)){
				continue;
			}
			if (str_ireplace("-thm", "", $national_name, $count)){
				$national_name = str_ireplace("-thm", "", $national_name);
			}
			if(strlen($national_name)<=5 && strlen($national_name)>=3){
				$national_name=$this->__formatNationalNumber($national_name, $country);
			}

			/**
			* 0 Location Number,
			* 1 Address,
			* 2 City,
			* 3 Province,
			* 4 Postal Code,
			* 5 Country,
			* 6 Phone,
			* 7 #FDMBMZ,
			* 8 TimsTV,
			* 9 Drive Thru,
			* 10 Print,
			*/
			$newdata = array();
			$location_num               = $this->__getValue($line, 0);
			$newdata['name']            = sprintf("%s-THM", $national_name);
			$newdata['address']         = $this->__getValue($line, 1);
			$newdata['city']            = ucwords($this->__getValue($line, 2));
			$newdata['province']        = (isset($p_lookup[strtoupper($this->__getValue($line, 3))])) ? $p_lookup[strtoupper($this->__getValue($line, 3))] : $this->__getValue($line, 3);
			$newdata['postal']          = strtoupper($this->__getValue($line, 4));
			$newdata['country']         = (isset($c_lookup[$line[5]])) ? $c_lookup[$line[5]] : $line[5];
			$newdata['phone']           = $this->__getValue($line, 6);
			$newdata['tz_id']           = (isset($t_lookup[$this->__getValue($line, 3)])) ? $tz_lookup[$t_lookup[$this->__getValue($line, 3)]] : $tz_lookup['Eastern'];
			$newdata['gp_id']           = sprintf("TIMH%d",$national_name);
			$newdata['mz_count']        = $this->__getValue($line, 7);
			$newdata['timstv_count']    = $this->__getValue($line, 8);
			$newdata['dt_count']        = $this->__getValue($line, 9);
			$newdata['print_count']     = $this->__getValue($line, 10);
			# Tim Hortons does not set static IPs
			#$newdata['ip_address']      = "1.1.1.0";
			#$newdata['ip_mask']         = "255.255.255.0";
			#$newdata['ip_gateway']      = "1.1.1.254";
			$location_instances[] = $newdata;


			$mz_count		    = $this->defaultVariable($newdata['mz_count']);
			$timstv_count		    = $this->defaultVariable($newdata['timstv_count']);
			$dt_count		    = $this->defaultVariable($newdata['dt_count']);
			$print_count		    = $this->defaultVariable($newdata['print_count']);

			$location_csv_ok = '1';
			$this->csv_ok = '1';
                        if ($mz_count >= '10'){
                                $this->debug(sprintf("Location [%s] has requested too many FMDBs [%d] !! Import will not continue.", $newdata['name'], $newdata['mz_count']), "INFO");
                                $newdata['mz_count'] = '0';
                                $mz_count_display = 'Too Many!';
                                $this->csv_ok = '0';
                                $location_csv_ok = '0';
                        } else {
                                $mz_count_display = $mz_count;
                        }

                        if ($timstv_count >= '3'){
                                $this->debug(sprintf("Location [%s] has requested too many Tims TVs [%d] !! Import will not continue.", $newdata['name'], $newdata['timstv_count']), "INFO");
                                $newdata['timstv_count'] = '0';
                                $timstv_count_display = 'Too Many!';
                                $this->csv_ok = '0';
                                $location_csv_ok = '0';
                        } else {
                                $timstv_count_display = $timstv_count;

                        }

                        if ($dt_count >= '3'){
                                $this->debug(sprintf("Location [%s] has requested too many Drive Thrus [%d] !! Import will not continue.", $newdata['name'], $newdata['dt_count']), "INFO");
                                $newdata['dt_count'] = '0';
                                $dt_count_display = 'Too Many!';
                                $this->csv_ok = '0';
                                $location_csv_ok = '0';
                        } else {
                                $dt_count_display = $dt_count;

                        }

                       if ($print_count >= '2'){
                                $this->debug(sprintf("Location [%s] has requested too many PRINTs [%d] !! Import will not continue.", $newdata['name'], $newdata['print_count']), "INFO");
                                $newdata['print_count'] = '0';
                                $print_count_display = 'Too Many!';
                                $this->csv_ok = '0';
                                $location_csv_ok = '0';
                        } else {
                                $print_count_display = $print_count;
                                
                        }

			if ($print_count && ($mz_count + $timstv_count + $dt_count)) {
				$this->debug(sprintf("Location [%s] cannot import a PRINT with any other marketing zone !! Import will not continue.", $newdata['name']), "INFO");
				$newdata['print_count'] = '0';
				$print_count_display = 'Not Alone!';
				$this->csv_ok = '0';
				$location_csv_ok = '0';
			}

                        if ($location_csv_ok == '1'){
                                echo sprintf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",str_pad(substr($newdata['name'],0,23),23), str_pad(substr($newdata['address'],0,23),23), str_pad(substr($newdata['city'],0,15),15), str_pad(substr($newdata['province'],0,10),10), str_pad(substr($newdata['country'],0,10),10),str_pad(substr($newdata['postal'],0,10),10), str_pad(substr($newdata['phone'],0,13),13), $mz_count_display, $timstv_count_display, $dt_count_display, $print_count_display);
                        } else {
                                echo sprintf("\33[01;31m%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\33[0m\n",str_pad(substr($newdata['name'],0,23),23), str_pad(substr($newdata['address'],0,23),23), str_pad(substr($newdata['city'],0,15),15), str_pad(substr($newdata['province'],0,10),10), str_pad(substr($newdata['country'],0,10),10),str_pad(substr($newdata['postal'],0,10),10), str_pad(substr($newdata['phone'],0,13),13), $mz_count_display, $timstv_count_display, $dt_count_display, $print_count_display);
                        }

		}
		$this->newinstances = $location_instances;
	}
	private function __formatNationalNumber($national_name, $country){
                $cc = ($country == "Canada") ? '1' : '9';  
		for($s=(strlen($national_name)+1);$s<=6;$s++){
			if($s==6) $national_name="$cc$national_name";
			else $national_name="0$national_name";
		}
		return $national_name;
	}
	private function __getValue($line,$c){
		if(isset($line[$c])) return trim($line[$c]);
		else return "";
	}
	/**
	 * Print activity to file
	 * @param string $level
	 * @param string $file
	 * @return boolean
	 */
	public function debug($message, $level="DEBUG", $filename="/opt/support_scripts/thm/thm_import_locations.log"){
		return error_log(date('Y-m-d H:i:s').": [$level] : $message\n", 3, $filename);
	}
	/**
	 * Case 00051894
	 * https://na13.salesforce.com/500a000000hmKnv
	 * @param array(8) $csv_data
	 */
	public function task1($csv_data){

		$this->debug("-----------------------------------------------------------------------------", "INFO");

		$this->debug("Validating csv data, converting to Location object.", "INFO");

		$this->formatCsv($csv_data);

                // Give the user the chance to determine if the script is doing the right thing:
                //   List the contents of the file.

                echo "\t\t(Note: fields are truncated for easy readablity.)\n\n";

                if ($this->csv_ok == '0'){
                        echo "\n\nOne or more of the entries in the CSV is invalid. Please check your CSV and try again.\n\n";
                        $this->debug(sprintf("Invalid CSV - Cancelling installation."), "INFO");
                        die();
                } else {
                        echo sprintf("\n\tDo you wish to continue with this import? (y/n) ");
                        $input = trim(fgets(STDIN));
                        if ($input != 'Y' && $input != 'y') {
                                echo sprintf("\tCancelling import\n");
                                $this->debug(sprintf("User Cancelled."), "INFO");
                                die();
                        }
                        echo sprintf("\tContinuing with the import\n");
                }

		$this->debug(sprintf("Import process started. working with %s records.", count($this->newinstances)), "INFO");

		foreach ($this->newinstances as $i=>$loc_instance){

			$this->current_instance_id = $i;

			// processing LOCATION
			$this->debug("-----------------------------------------------------------------------------", "INFO");
			$this->debug(sprintf("%s - Checking location existence [%s]", ($i+1), $loc_instance['name']), "INFO");
			echo sprintf("%s - Checking location existence [%s]", ($i+1), $loc_instance['name']);
			if(!$this->checkLocation($loc_instance)){

				echo sprintf("\n   Creating new location [%s]", $loc_instance['name']);
				$this->debug(sprintf("Creating new location [%s]", $loc_instance['name']), "INFO");
				if($this->addLocation()){

					$this->debug(sprintf("+ Location [%s] created. ID=%s", $loc_instance['name'], $this->getId()), "INFO");
				}
				else {

					$this->debug("* Can't create location", "ERROR");
				}
			}
			else {
				echo sprintf("\n   Location [%s] already exists...checking configuration:", $loc_instance['name']);
				$this->debug("Location already exists.", "INFO");
				// print differences
				$this->printDiff($this->newinstances[$this->current_instance_id]);
			}

			// processing CONNECTION only if asked for DMB MZs
                        if ($loc_instance['mz_count'] > 0) {
			  $this->debug(sprintf("Checking connection for location [%s]", $loc_instance['name']), "INFO");
			  if(!$this->checkConnection("DMB Connection")){

				  echo sprintf("\n\tAdding Main DMB connection.");
				  $this->debug(sprintf("Adding connection for location [%s]", $loc_instance['name']), "INFO");
				  if($this->addConnection("DMB Connection")){

					  $this->debug(sprintf("+ Connection created under [%s] location.", $loc_instance['name']), "INFO");

				  }
				  else {

					  $this->debug("* Can't create connection", "ERROR");
				  }
			  }
			  else {
				  echo sprintf("\n\tMain DMB connection exists.");
				  $this->debug("Connection already configured", "INFO");
			  }
			}

			// processing MARKETING ZONES only if asked for DMB MZs
			$this->debug(sprintf("Processing MZs for location [%s]", $loc_instance['name']), "INFO");

			if ($loc_instance['mz_count'] > 0) {
			  if($this->addMZs()){

				  $this->debug("MZs processed.", "INFO");

			  }
			  else {

				  $this->debug("* Can't create marketing zones", "ERROR");
			  }
			}

			// NEW - Check if TimsTV, Drive Thru, or Print Connections + MZ are required
				
			

			if($loc_instance['timstv_count'] >= 1){
                                $conn_name = "Dining Room";
                                $mz_name = "CH 1 - TimsTV Dining Room";
				$this->debug(sprintf("Checking '$conn_name' for location [%s]", $loc_instance['name']), "INFO");

                                $cafe_conn = $this->checkConnection($conn_name, false);

                                if(!$cafe_conn) {

					echo sprintf("\n\tAdding %s connection.", $conn_name);
                                        $this->debug(sprintf("Adding '$conn_name' for location [%s]", $loc_instance['name']), "INFO");

                                        $cafe_conn = $this->addConnection($conn_name, false);

                                        if($cafe_conn){

                                                $this->debug(sprintf("+ %s created under [%s] location.", $conn_name, $loc_instance['name']), "INFO");

                                        }
                                        else {

                                                $this->debug("* Can't create connection", "ERROR");
                                        }
                                }
                                else {

					echo sprintf("\n\t%s connection exists.", $conn_name);
                                        $this->debug("Connection already configured", "INFO");
                                }
                                // processing MARKETING ZONES
                                $this->debug(sprintf("Creating '$mz_name' MZs [%s]", $loc_instance['name']), "INFO");
                                if($this->addMZs($cafe_conn->getId(), $loc_instance['timstv_count'], $mz_name)){

                                        $this->debug("MZs processed.", "INFO");

                                }
                                else {

                                        $this->debug("* Can't create $mz_name", "ERROR");
                                }

			}	
			if($loc_instance['dt_count'] >= 1){
                                $conn_name = "Drive Thru";
                                $mz_name = "DT Presell";
				$this->debug(sprintf("Checking '$conn_name' for location [%s]", $loc_instance['name']), "INFO");

                                $cafe_conn = $this->checkConnection($conn_name, false);

                                if(!$cafe_conn) {

					echo sprintf("\n\tAdding %s connection.", $conn_name);
                                        $this->debug(sprintf("Adding '$conn_name' for location [%s]", $loc_instance['name']), "INFO");

                                        $cafe_conn = $this->addConnection($conn_name, false);

                                        if($cafe_conn){

                                                $this->debug(sprintf("+ %s created under [%s] location.", $conn_name, $loc_instance['name']), "INFO");

                                        }
                                        else {

                                                $this->debug("* Can't create connection", "ERROR");
                                        }
                                }
                                else {
					echo sprintf("\n\t%s connection exists.", $conn_name);
                                        $this->debug("Connection already configured", "INFO");
                                }
                                // processing MARKETING ZONES
                                $this->debug(sprintf("Creating '$mz_name' MZs [%s]", $loc_instance['name']), "INFO");
                                if($this->addMZs($cafe_conn->getId(), $loc_instance['dt_count'], $mz_name)){

                                        $this->debug("MZs processed.", "INFO");

                                }
                                else {

                                        $this->debug("* Can't create $mz_name", "ERROR");
                                }

			}

			if($loc_instance['print_count'] >= 1){
				$conn_name = "Print";
                                $mz_name = "Print";
				$this->debug(sprintf("Checking '$conn_name' for location [%s]", $loc_instance['name']), "INFO");

                                $cafe_conn = $this->checkConnection($conn_name, false);

                                if(!$cafe_conn) {

					echo sprintf("\n\tAdding %s connection.", $conn_name);
                                        $this->debug(sprintf("Adding '$conn_name' for location [%s]", $loc_instance['name']), "INFO");

                                        $cafe_conn = $this->addConnection($conn_name, false);

                                        if($cafe_conn){

                                                $this->debug(sprintf("+ Main Connection created under [%s] location.", $loc_instance['name']), "INFO");

                                        }
                                        else {

                                                $this->debug("* Can't create connection", "ERROR");
                                        }
                                }
                                else {

					echo sprintf("\n\t%s connection exists.", $conn_name);
                                        $this->debug("Connection already configured", "INFO");
                                }
                                // processing MARKETING ZONES
                                $this->debug(sprintf("Creating '$mz_name' MZs [%s]", $loc_instance['name']), "INFO");
                                if($this->addMZs($cafe_conn->getId(), 1, $mz_name)){

                                        $this->debug("MZs processed.", "INFO");

                                }
                                else {

                                        $this->debug("* Can't create $mz_name", "ERROR");
                                }

			}

			
			$this->debug(sprintf("Location [%s] processed successfully.", $loc_instance['name']), "INFO");
			echo sprintf("\n   Location [%s] processed successfully.\n", $loc_instance['name']);
		}
	}
}
// start

if($argc == 2) {

        // two arguments - filename + csv file
        $csvfile = $argv[1];

} elseif($argc == 3) {
        // three arguments -- double check the location!

        $csvfile = $argv[2];
        echo "***************************************************************************************************\n";
        echo "*\tPlease note that entering the client_id is no longer required!!\n";
        echo "*\tThis syntax is accepted for backward compatibility.  Using hardcoded client_id: ".$clientid."\n";
        echo "***************************************************************************************************\n";

} else {
        echo "\nSyntax:  php ".$argv[0]." input_file.csv\n";
        echo "\n  Client_id ".$clientid." is hard coded into this script.\n";
        echo "\n  Previous syntax: php ".$argv[0]." input_file.csv has been maintained for backwards compatability.\n\n";
        die();
        
}

echo "\nImporting Location(s) for ".$clientname.".\n\n";
echo "  Please verify CSV contents: \n\n";


$ss_loc = new SS_Location($clientid);// Support Script Location

$csv_data = array();

try{
	$handle = fopen($csvfile, "r");
	while(($data = fgetcsv($handle)) != FALSE){
		$csv_data[] = $data;
	}
	fclose($handle);
} catch(Exception $e){
	die("I/O Problems: ".$e->getMessage());
}

$ss_loc->task1($csv_data);

if($ss_loc->hasError()) die("WEBUI-error: ". $ss_loc->getError()."\n\n");

echo "\n\n";
// end
