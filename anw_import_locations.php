<?php

require_once('/opt/support_scripts/location_imports/location_imports_common.php');

/**
 * Support Scripts Location : Location Import
 * Works for A&W clients only
 * @author jperez
 * Editted by JPost
*/

# Configuration unique to each client:
$clientid = '318';
$clientname = 'A&W';
$cas_email = 'janet.post@cineplex.com,felipe.alvarado@cineplex.com';

# Variables that should be the same for all clients:
$options = getopt("n");
$logfiledir = dirname(__FILE__) . '/' . basename(__FILE__, '.php') . '.log';


class SS_Location extends SS_Location_Common {
	protected $newinstances;
	protected $current_instance_id;

	protected $user_id;
	protected $client_id;

	protected $connection;
	protected $mzs;

        // for use with emails
        protected $mzsetup;

        /**
         * Startup application
         */

        public function __construct($client_id,$logfiledir) {
                parent::__construct($client_id,$logfiledir);
        }

	/**
	 * Create new Marketing Zone
	 * @param number $num
	 * @param string $netMask
	 * @param string $getway
	 * @param string $ip
	 * @return boolean
	 */

	public function addMZs($conn_id=NULL, $mz_count=NULL, $mz_name=NULL) {

		$resource_ids = $this->getPlaceholderList();
		$current_serial = $this->placeholdersInit();
		$type = $this->getEntity('device type', array('name' => 'Placeholder'));
		$model = $this->getEntity('device model', array('name' => 'ME3'));
		$os = $this->getEntity('os info', array('name' => 'Xpress-c3_ph-1_0_0-RELEASE'));
		if(empty($conn_id)) $conn_id = $this->connection->getId();
		if(empty($mz_count)) $mz_count=$this->newinstances[$this->current_instance_id]['mz_count'];
		$ip=$this->newinstances[$this->current_instance_id]['ip_address'];
		$lastip=$this->newinstances[$this->current_instance_id]['last_ip'];

		$check_existence=FALSE;

		$mzs = new WebUI_MarketingZoneList($this->getDB());
		$mzs->addWhere('connection', $conn_id);
		$mzs->load();
		if($mzs->count()>0)$check_existence=TRUE;

                // Determining if the CSV is asking for more MZ's then are currently set up for an already
	        // existing location.

                $i = 1;
                if ($mzs->count() > $mz_count) $i = ($mz_already_set_up + 1);
                if ($mzs->count() >= $mz_count){
                        echo "\n\t\tMZ's are already set up for this connection.";
                        $this->debug(sprintf("There are already %d MZ(s) set up for this connection. %d were requested. Nothing to do.", $mzs->count(), $mz_count), "INFO");
                        return 1;
                }
     
                $mz_basename = $mz_name;
		$MB = 1;
		for($i=$i;$i<=$mz_count;$i++) {
			// create MZ
			if(empty($mz_name)) {
				if ($i != ($mz_count - 1)) {
					$mz_name = sprintf("Screen %d - MB%d", $i, $MB);
					$MB++;
				} elseif ($i == ($mz_count - 1)) {
				$mz_name = sprintf("Screen %d - Promo", $i);
				}
			}
			$mz = new WebUI_MarketingZone($this->getDB());
			$mz->setClientId($this->client_id);
			$mz->setConnectionId($conn_id);
			$mz->setName($mz_name);
			// check existing MZ
			$existing_mz = $mzs->findValue('getName', $mz_name);
			if($check_existence && !empty($existing_mz)){
				// log duplicate configuration
				$old_ip   = $existing_mz->getNetworkAddress();
				$old_mask = $existing_mz->getNetworkMask();
				$old_gate = $existing_mz->getNetworkGateway();
				$this->debug("$mz_name already configured for this connection.\n\tIP=$old_ip\n\tMASK=$old_mask\n\tGATE=$old_gate", "INFO");
				$mz_name=NULL;
				continue;
			} else{
				if(!preg_match("/Feature/", $mz_name)){
					$ip_add = $this->setIP($i, $ip, 0);
					$ip_gate = $this->newinstances[$this->current_instance_id]['ip_gateway'];
					$ip_mask = $this->newinstances[$this->current_instance_id]['ip_mask'];
					$mz->setNetworkAddress($ip_add);
					$mz->setNetworkGateway($ip_gate);
					$mz->setNetworkMask($ip_mask);
					$this->debug("Setting $mz_name as next:\n\tIP=$ip_add\n\tMASK=$ip_mask\n\tGATE=$ip_gate", "INFO");
				}
			}
			$lom = new WebUI_LocationOperationManage($this->getDB());
			$lom->load($this->getId());
			if(!$mz->validate()){
				$this->debug("MZ Validation failure", "ERROR");break;
			}
			if(!$mz->save($this->user_id)){
				$this->debug("Problems creating MZ", "ERROR");
				return false;
			}
			$apply = new WebUI_OperationApply($this->getDB());
			$apply->setOperationManager($lom->getOperationManager());
			$user = new WebUI_User($link);
			$user->load($this->user_id);
			if(!$apply->applyToMZ($mz, $user)) {
				$this->debug("Problems creating MZ", "ERROR");
				return false;
			}
			// setting up master/slave
			$playType='MASTER';
			if($i>1)$playType=sprintf("SLAVE %d",($i-1));
			$mz->setPlayType($playType);$this->debug("Setting up $mz_name as $playType", "INFO");
			$op = $mz->getOperationManager();
			// setting display schedule = Always On - ref:/opt/WebUI/lib/WebUI_OperationManage.php
			$op->setDisplayTimes(array(array('on'  => '00:00:00','off' => '00:00:00','days'=> 127)));
			// setting election enabled  = Yes and NO for McCafe MZs
			if(preg_match("/Cafe/", $mz_name)) $op->setElectionEnabled(0);
			else $op->setElectionEnabled(0);
			$op->setPlaybackMode('BLIPLESS');
			if(!$op->save())$this->debug("Problems updating MZOperationManager", "ERROR");

			$mz->setOperationManager($op);
			if(!$mz->save($this->user_id))$this->debug("Problems updating MZ", "ERROR");
			// sending IP package
			$static_package = new WebUI_StaticNetworkPackage($this->getDB());
			$static_package->build($mz, $this->user_id);
			// display configuration
			$display = new WebUI_MZDisplayConfiguration($this->getDB());
			$display->load($mz->getId());
			// setting screentype to 1 = LG (LG, Samsung, Sharp, etc.)
			$display->setDisplayConfiguration(1, 'ON', 'LOCKED', 80);
			if(!$display->save())$this->debug("Problems updating MZDisplayConfiguration", "ERROR");
			// adding place holder to new MZ
			$this->debug(sprintf("Associating placeholders for MZ [%s]", $mz->getName()), "INFO");
			$dev_id = 0;
			if (count($resource_ids) == 0) {
				$this->debug(sprintf("Creating new placeholder [FD1-%05d]", $current_serial), "INFO");
				$dev_id = $this->getPlaceholderId($current_serial, $os, $model, $type);
				$current_serial++;
			} else {
				$this->debug(sprintf("Loading unused fake-device 1 of %d", count($resource_ids)), "INFO");
				// get fake device from list
				$dev_id = array_pop($resource_ids);
			}
			if(!$this->addPlaceholder($mz, $dev_id, $user)){
				$this->debug("\tCan't create placeholders", "ERROR");
			}
			$this->debug(sprintf("[ID:%d] attached to [%s]", $dev_id, $mz->getName()), "INFO");
			$mz_name=NULL;
		}
		return true;
	}

	/**
	 * Process CSV values and format it to Array(Locations)
	 * @param Array $data
	 * @return Array(Locations)
	 */
	public function formatCsv($data){
                // Setting up some variables

                $location_instances = array();
                $p_lookup = $this->getArrayProvinces();
                $t_lookup = $this->getArrayTimezones();
                $c_lookup = $this->getArrayCountries();
                $existingLocationsArray = array();

		$timezones = new WebUI_TimezoneList($this->getDB());
		$timezones->load();
		$tz_lookup = array();
		while($tz = $timezones->fetchItem())$tz_lookup[$tz->getName()] = $tz->getId();
                echo "Location Name\t\tAddress\t\t\tCity\t\tProv/State\tCountry\t\tPostal/Zip\tPhone\t\tFDMBs\tPromos\n";
                echo "-------------\t\t-------\t\t\t----\t\t----------\t-------\t\t----------\t-----\t\t-----\t------\n";
		foreach($data as $i=>$line) {
			if ($i == 0)continue;// skip headers

			$national_name=trim($this->__getValue($line, 0));

                        // Need to find the country value so we can properly set the national name.
                        //   If the location is Canadian, than the location ID is 1#####
                        //   if the location is American, than the location ID is 9#####
			//   if the location is in the Carribean, than the location ID is 4#####

			$country  = (isset($c_lookup[$this->__getValue($line,5)])) ? $c_lookup[$this->__getValue($line,5)] : $this->__getValue($line,5);
                        $country  = (isset($c_lookup[$line[5]])) ? $c_lookup[$line[5]] : $line[5];

			// Validate national name & Format nation name
			if(empty($national_name)){
				continue;
			}
			if(strlen($national_name)<=5 && strlen($national_name)>=3){
				$national_name=$this->__formatNationalNumber($national_name, $country);
			}
			/**
			* 0 Location Number,
			* 1 Address,
			* 2 City,
			* 3 State,
			* 4 Zip/Postal,
			* 5 Country,
			* 6 Phone Number,
			* 7 DMB,
			* 8 Brand TV,
			*/
			$newdata = array();
                        $location_num               = $this->__getValue($line, 0);
                        $newdata['address']         = $this->__getValue($line, 1);
                        $newdata['city']            = ucwords(strtolower($this->__getValue($line, 2)));
                        $newdata['province']        = (isset($p_lookup[strtoupper($this->__getValue($line, 3))])) ? $p_lookup[strtoupper($this->__getValue($line, 3))] : $this->__getValue($line, 3);
                        $newdata['country']         = (isset($c_lookup[$this->__getValue($line, 5)])) ? $c_lookup[$this->__getValue($line, 5)] : $this->__getValue($line, 5);
                        $newdata['postal']          = strtoupper($this->__getValue($line, 4));
                        $newdata['phone']           = $this->__getValue($line, 6);
                        $newdata['tz_id']           = (isset($t_lookup[$this->__getValue($line, 3)])) ? $tz_lookup[$t_lookup[$this->__getValue($line, 3)]] : $tz_lookup['Eastern'];
                        $newdata['gp_id']           = sprintf("AWFO%s",$national_name);
                        $newdata['mz_count']        = $this->__getValue($line, 7);
                        $newdata['fp_count']        = $this->__getValue($line, 8);
                        $newdata['ip_address']      = "1.1.1.0";
                        $newdata['last_ip']         = "129";
                        $newdata['ip_mask']         = "255.255.255.0";
                        $newdata['ip_gateway']      = "1.1.1.254";
                        $newdata['name']            = sprintf("%s-AWFO-%s", $national_name, $newdata['city']);

                        // We will set the initial value for whether the CSV is formatted correctly to 'yes'. We will check to see if it is broken later.
                        //   And, yes, I know that GOOD programmers do this the other way to catch unforeseen errors.  I am not a good programmer.      
                        $location_csv_ok = '1';

                        // This checks to see if the location already exists and fills an array with all sorts of cool information -- most notably the IP addresses in use.
                        $existingLocationsArray = $this->populateExistingLocationsArray($newdata['name'],$existingLocationsArray,$newdata['ip_address']);

                        // Check to see if there are already IP addresses in use and put that into the $newdata varible for use later.
                        $newdata['last_ip'] = $existingLocationsArray[$newdata['name']]['lastip'] ? $existingLocationsArray[$newdata['name']]['lastip'] : $newdata['last_ip'];
                        $location_instances[] = $newdata;

			// Make sure the number of MZ's and Feature Panels are sane:
                        if ($newdata['mz_count']  >= '10'){
                                $this->debug(sprintf("Location [%s] has requested too many FMDBs [%d] !! Import will not continue.", $newdata['name'], $newdata['mz_count']), "INFO");
                                $newdata['mz_count'] = '0';
                                $mz_count_display = 'Too Many!';
                                $this->csv_ok = '0';
                                $location_csv_ok = '0';
                        } else {
                                $mz_count_display = $newdata['mz_count'];
                        }

                        if ($newdata['fp_count']  >= '2'){
                                $this->debug(sprintf("Location [%s] has requested too many Feature Panels [%d] !! Import will not continue.", $newdata['name'], $newdata['promo_count']), "INFO");
                                $newdata['fp_count'] = '0';
                                $fp_count_display = 'Too Many!';
                                $this->csv_ok = '0';
                                $location_csv_ok = '0';
                        } else {
                                $fp_count_display = $newdata['fp_count'];

                        }

                        $mz_count                   = $newdata['mz_count'];
                        $fp_count                   = $newdata['fp_count'];

                        // Make sure that the Countries exist -- so it will prepend the correct location code to the location number

                        if (! in_array($newdata['country'], $c_lookup)) {
                                 $location_csv_ok = '0';
                                 $country_display = "** " . $newdata['country'];
                        } else {
                                 $country_display = $newdata['country'];
                        }


                        if ($location_csv_ok == '1'){
                                echo sprintf("%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",$newdata['name'], str_pad(substr($newdata['address'],0,23),23), str_pad(substr($newdata['city'],0,15),15), str_pad(substr($newdata['province'],0,10),10), str_pad(substr($country_display,0,10),10),str_pad(substr($newdata['postal'],0,10),10), str_pad(substr($newdata['phone'],0,13),13), $mz_count_display, $fp_count_display);
                        } else {
                                echo sprintf("\33[01;31m%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\t%s\33[0m\n",$newdata['name'], str_pad(substr($newdata['address'],0,23),23), str_pad(substr($newdata['city'],0,15),15), str_pad(substr($newdata['province'],0,10),10), str_pad(substr($country_display,0,10),10),str_pad(substr($newdata['postal'],0,10),10), str_pad(substr($newdata['phone'],0,13),13), $mz_count_display, $fp_count_display);
                        }
		}
		$this->newinstances = $location_instances;
	}

	public function task1($csv_data){

		$this->debug("-----------------------------------------------------------------------------", "INFO");

		$this->debug("Validating csv data, converting to Location object.", "INFO");

		$this->formatCsv($csv_data);
		$this->locationlist = array();

                // Give the user the chance to determine if the script is doing the right thing:
                //   List the contents of the file.

                echo "\t\t(Note: fields are truncated for easy readablity.)\n\n";
	
                if ($this->csv_ok == '0'){
                        echo "\n\nOne or more of the entries in the CSV is invalid. Please check your CSV and try again.\n\n";
                        $this->debug(sprintf("Invalid CSV - Cancelling installation."), "INFO");
                        die();
                } else {
                        echo sprintf("\n\tDo you wish to continue with this import? (y/n) ");
                        $input = trim(fgets(STDIN));
                        if ($input != 'Y' && $input != 'y') {
                                echo sprintf("\tCancelling import\n");
                                $this->debug(sprintf("User Cancelled."), "INFO");
                                die();
                        }
                        echo sprintf("\tContinuing with the import\n");
                }

		$this->debug(sprintf("Import process started. working with %s records.", count($this->newinstances)), "INFO");

		foreach ($this->newinstances as $i=>$loc_instance){

			$this->current_instance_id = $i;
			$mz_name = '';

			// processing LOCATION
			$this->debug("-----------------------------------------------------------------------------", "INFO");
			$this->debug(sprintf("%s - Checking location existence [%s]", ($i+1), $loc_instance['name']), "INFO");
			if(!$this->checkLocation($loc_instance)){

				echo sprintf("\n   Creating new location [%s]", $loc_instance['name']);
				$this->debug(sprintf("Creating new location [%s]", $loc_instance['name']), "INFO");
				if($this->addLocation()){

					$this->debug(sprintf("+ Location [%s] created. ID=%s", $loc_instance['name'], $this->getId()), "INFO");
					$this->locationlist[] = $loc_instance['name'];
				}
				else {

					$this->debug("* Can't create location", "ERROR");
				}
			}
			else {
				echo sprintf("\n   Location [%s] already exists...checking configuration:", $loc_instance['name']);
				$this->debug("Location already exists.", "INFO");
				// print differences
				$this->printDiff($this->newinstances[$this->current_instance_id]);
			}

                        if($loc_instance['mz_count'] > 0) {

                                $conn_name = "FDMB Connection";
                                $connection = $this->ProcessConnection($conn_name,$loc_instance);

                                $this->ProcessMZs($connection,$conn_name,$mz_name,$loc_instance['mz_count'],$loc_instance);
                        }

                        if($loc_instance['fp_count'] == 1) {

                                $conn_name = "Main Connection";
                                $connection = $this->ProcessConnection($conn_name,$loc_instance);

                                $mz_name = "Feature Panel";
                                $this->ProcessMZs($connection,$conn_name,$mz_name,$loc_instance['fp_count'],$loc_instance);
                        }

                        $this->debug(sprintf("Location [%s] processed successfully.", $loc_instance['name']), "INFO");
                        echo sprintf("\n   Location [%s] processed successfully.\n", $loc_instance['name']);
               }
        }
}

// start

if($argc == 2) {

        // two arguments - filename + csv file
        $csvfile = $argv[1];

} elseif($argc == 3 && array_key_exists('n', $options) ) {
        // three arguments - filename + csv file + -n argument
        $csvfile = $argv[1];

} elseif($argc == 3 && ! array_key_exists('n', $options) ) {
        // three arguments -- double check the location!

        $csvfile = $argv[2];
        echo "***************************************************************************************************\n";
        echo "*\tPlease note that entering the client_id is no longer required!!\n";
        echo "*\tThis syntax is accepted for backward compatibility.  Using hardcoded client_id: ".$clientid."\n";
        echo "***************************************************************************************************\n";

} else {
        echo "\tSyntax:  php ".$argv[0]." input_file.csv [-n]\n";
        echo "\t  Client_id ".$clientid." is hard coded into this script.\n";
        echo "\t  -n  Disables email notification to CAS upon completion of script\n";
        echo "\t  Previous syntax: php ".$argv[0]." input_file.csv has been maintained for backwards compatability.\n\n";
        die();

}

echo "\nImporting Location(s) for ".$clientname.".\n\n";
echo "  Please verify CSV contents: \n\n";
if (! array_key_exists('n', $options)) {
        echo sprintf("   (An email will be sent to %s after import.  If you don't want this, cancel and rerun the script with '-n')\n\n",$cas_email);
} else {
	echo "    (Email notification to CAS is TURNED OFF.  If you want an email to be sent to the CAS team, please remove '-n' from the commandline.)\n\n";
}


$ss_loc = new SS_Location($clientid,$logfiledir);// Support Script Location

$csv_data = array();

try{
	$handle = fopen($csvfile, "r");
	while(($data = fgetcsv($handle)) != FALSE){
		$csv_data[] = $data;
	}
	fclose($handle);
} catch(Exception $e){
	die("I/O Problems: ".$e->getMessage());
}

$ss_loc->task1($csv_data);


if($ss_loc->hasError()) die("WEBUI-error: ". $ss_loc->getError()."\n\n");

if (! array_key_exists('n', $options)) {
	echo sprintf("\n\tSending email to %s.\n", $cas_email);
	try{
		$ss_loc->email_cas($cas_email,$clientname);
	} catch(Exception $e){
		warn("Email could not be sent: ".$e->getMessage());
	}
}

echo "\n\n";

// end
